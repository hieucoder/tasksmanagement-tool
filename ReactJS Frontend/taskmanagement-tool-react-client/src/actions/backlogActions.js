import axios from 'axios';
import { GET_ERRORS, GET_BACKLOG, GET_TASK, DELETE_TASK } from './types';

export const addTask = (backlog_id, task, history) => async dispatch => {
    try {
        await axios.post(`/api/backlogs/${backlog_id}`, task);
        history.push(`/project-board/${backlog_id}`);
        dispatch({
            type: GET_ERRORS,
            payload: {}
        });
    } catch (error) {
        dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        });
    }    
};

export const getBacklog = backlog_id => async dispatch => {
    try {
        const response = await axios.get(`/api/backlogs/${backlog_id}`);
        dispatch({
            type: GET_BACKLOG,
            payload: response.data
        });
        console.log(response.data);
    } catch (error) {
        dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        });
    }
};

export const getTask = (backlog_id, task_id, history) => async dispatch => {
    try {
        const response = await axios.get(`/api/backlogs/${backlog_id}/${task_id}`);
        dispatch({
            type: GET_TASK,
            payload: response.data
        });
    } catch (error) {
        history.push('/dashboard');
    }
};

export const updateTask = (backlog_id, task_id, project_task, history) => async dispatch => {
    try {
        await axios.patch(`/api/backlogs/${backlog_id}/${task_id}`, project_task);
        history.push(`/project-board/${backlog_id}`);
        dispatch({
            type: GET_ERRORS,
            payload: {}
        })
    } catch (error) {
        dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        })
    }
};

export const deleteTask = (backlog_id, task_id) => async dispatch => {
    if (window.confirm(`Are you sure that you want to delete the task with identifier ${task_id}?\nOnce deleted, this action can not be undone!`)) {
        await axios.delete(`/api/backlogs/${backlog_id}/${task_id}`);
        dispatch({
            type: DELETE_TASK,
            payload: task_id
        });
    }
};