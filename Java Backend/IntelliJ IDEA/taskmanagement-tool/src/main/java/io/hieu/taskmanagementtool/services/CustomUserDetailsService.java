package io.hieu.taskmanagementtool.services;

import io.hieu.taskmanagementtool.domain.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface CustomUserDetailsService extends UserDetailsService {
    User loadUserById(Long id);

    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}