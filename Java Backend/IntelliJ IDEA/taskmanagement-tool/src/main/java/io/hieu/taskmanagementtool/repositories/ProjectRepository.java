package io.hieu.taskmanagementtool.repositories;

import io.hieu.taskmanagementtool.domain.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {
    Project getById(Long id);

    Project getByProjectIdentifier(String identifier);

    Project getByProjectName(String projectName);

    Project getByDescription(String description);

    @Override
    Iterable<Project> findAll();

    Iterable<Project> findAllByProjectLeader(String username);
}