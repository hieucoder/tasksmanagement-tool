package io.hieu.taskmanagementtool.web;

import io.hieu.taskmanagementtool.domain.Task;
import io.hieu.taskmanagementtool.services.ErrorValidationService;
import io.hieu.taskmanagementtool.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(path = {"/api/backlogs", "/api/backlogs.html", "/api/backlogs/index", "/api/backlogs/index.html"})
@CrossOrigin
public class BacklogController {
    @Autowired
    private TaskService taskService;

    @Autowired
    private ErrorValidationService errorValidationService;

    @PostMapping(path = {"/{backlog_id}", "/{backlog_id}/", "/{backlog_id}/index", "/{backlog_id}/index/", "/{backlog_id}/index.html", "/{backlog_id}/index.html/"})
    public ResponseEntity<?> addTaskToBacklog(@Valid @RequestBody Task task, BindingResult bindingResult, @PathVariable String backlog_id, Principal principal) {
        ResponseEntity<?> errors = errorValidationService.ErrorValidationService(bindingResult);
        if (errors != null) {
            return errors;
        }
        Task newTask = taskService.addTask(backlog_id, task, principal.getName());
        return new ResponseEntity<>(newTask, HttpStatus.CREATED);
    }

    @GetMapping(path = {"/{backlog_id}", "/{backlog_id}/", "/{backlog_id}/index", "/{backlog_id}/index/", "/{backlog_id}/index.html", "/{backlog_id}/index.html/"})
    public Iterable<Task> getBacklog(@PathVariable String backlog_id, Principal principal) {
        return taskService.findBacklogById(backlog_id, principal.getName());
    }

    @GetMapping(path = {"/{backlog_id}/{task_id}", "/{backlog_id}/{task_id}/", "/{backlog_id}/{task_id}/index", "/{backlog_id}/{task_id}/index/", "/{backlog_id}/{task_id}/index.html", "/{backlog_id}/{task_id}/index.html/"})
    public ResponseEntity<?> getTask(@PathVariable String backlog_id, @PathVariable String task_id, Principal principal) {
        Task task = taskService.findTaskByProjectSequence(backlog_id, task_id, principal.getName());
        return new ResponseEntity<Task>(task, HttpStatus.OK);
    }

    @PatchMapping(path = {"/{backlog_id}/{task_id}", "/{backlog_id}/{task_id}/", "/{backlog_id}/{task_id}/index", "/{backlog_id}/{task_id}/index/", "/{backlog_id}/{task_id}/index.html", "/{backlog_id}/{task_id}/index.html/"})
    public ResponseEntity<?> updateTask(
            @Valid @RequestBody Task task,
            BindingResult bindingResult,
            @PathVariable String backlog_id,
            @PathVariable String task_id,
            Principal principal) {
        ResponseEntity<?> errors = errorValidationService.ErrorValidationService(bindingResult);
        if (errors != null) {
            return errors;
        }
        Task updatedTask = taskService.updateByProjectSequence(task, backlog_id, task_id, principal.getName());
        return new ResponseEntity<Task>(updatedTask, HttpStatus.OK);
    }

    @DeleteMapping(path = {"/{backlog_id}/{task_id}", "/{backlog_id}/{task_id}/", "/{backlog_id}/{task_id}/index", "/{backlog_id}/{task_id}/index/", "/{backlog_id}/{task_id}/index.html", "/{backlog_id}/{task_id}/index.html/"})
    public ResponseEntity<?> deleteTask(@PathVariable String backlog_id, @PathVariable String task_id, Principal principal) {
        taskService.deleteTaskByProjectSequence(backlog_id, task_id, principal.getName());
        return new ResponseEntity<String>(String.format("Task with identifier %s is removed from the database!", task_id), HttpStatus.OK);
    }
}