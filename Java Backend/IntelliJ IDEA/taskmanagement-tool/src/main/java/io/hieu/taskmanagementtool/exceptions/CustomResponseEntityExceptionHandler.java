package io.hieu.taskmanagementtool.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler
    public final ResponseEntity<Object> handleProjectIdException(ProjectIdException projectIdException, WebRequest webRequest) {
        ProjectIdExceptionResponse projectIdExceptionResponse = new ProjectIdExceptionResponse(projectIdException.getMessage());
        return new ResponseEntity(projectIdExceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleProjectNotFoundException(ProjectNotFoundException projectNotFoundException, WebRequest webRequest) {
        ProjectNotFoundExceptionResponse projectNotFoundExceptionResponse = new ProjectNotFoundExceptionResponse(projectNotFoundException.getMessage());
        return new ResponseEntity(projectNotFoundExceptionResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleNoProjectsExistInDatabaseException(NoProjectsExistInDatabaseException noProjectsExistInDatabaseException, WebRequest webRequest) {
        NoProjectsExistsInDatabaseExceptionResponse noProjectsExistsInDatabaseExceptionResponse = new NoProjectsExistsInDatabaseExceptionResponse(noProjectsExistInDatabaseException.getMessage());
        return new ResponseEntity(noProjectsExistsInDatabaseExceptionResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleProjectNameException(ProjectNameException projectNameException, WebRequest webRequest) {
        ProjectNameExceptionResponse projectNameExceptionResponse = new ProjectNameExceptionResponse(projectNameException.getMessage());
        return new ResponseEntity(projectNameExceptionResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleProjectDescriptionException(ProjectDescriptionException projectDescriptionException, WebRequest webRequest) {
        ProjectDescriptionExceptionResponse projectDescriptionExceptionResponse = new ProjectDescriptionExceptionResponse(projectDescriptionException.getMessage());
        return new ResponseEntity(projectDescriptionExceptionResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleUsernameAlreadyExistsException(UsernameAlreadyExistsException usernameAlreadyExistsException, WebRequest webRequest) {
        UsernameAlreadyExistsExceptionResponse usernameAlreadyExistsExceptionResponse = new UsernameAlreadyExistsExceptionResponse(usernameAlreadyExistsException.getMessage());
        return new ResponseEntity(usernameAlreadyExistsExceptionResponse, HttpStatus.BAD_REQUEST);
    }
}