package io.hieu.taskmanagementtool.services.impl;

import io.hieu.taskmanagementtool.domain.Backlog;
import io.hieu.taskmanagementtool.domain.Project;
import io.hieu.taskmanagementtool.domain.User;
import io.hieu.taskmanagementtool.exceptions.*;
import io.hieu.taskmanagementtool.repositories.BacklogRepository;
import io.hieu.taskmanagementtool.repositories.ProjectRepository;
import io.hieu.taskmanagementtool.repositories.UserRepository;
import io.hieu.taskmanagementtool.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private BacklogRepository backlogRepository;

    @Autowired
    private UserRepository userRepository;

    // INSERT data into database function(s):
    @Override
    public Project saveOrUpdateProject(Project project, String username) {
        if (project.getId() != null) {
            Project existingProject = projectRepository.getByProjectIdentifier(project.getProjectIdentifier());
            if (existingProject != null && (!existingProject.getProjectLeader().equals(username))) {
                throw new ProjectNotFoundException(String.format("Project with identifier %s is not found in your account!", project.getProjectIdentifier()));
            } else if (existingProject == null) {
                throw new ProjectNotFoundException(String.format("Project with identifier %s is not found in your account!", project.getProjectIdentifier()));
            }
        }

        try {
            User user = userRepository.findByUsername(username);
            project.setUser(user);
            project.setProjectLeader(user.getUsername());
            project.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());

            if (project.getId() == null) {
                Backlog backlog = new Backlog();
                project.setBacklog(backlog);
                backlog.setProject(project);
                backlog.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
            }

            if (project.getId() != null) {
                project.setBacklog(backlogRepository.findByProjectIdentifier(project.getProjectIdentifier().toUpperCase()));
            }

            return projectRepository.save(project);
        } catch (Exception exception) {
            java.lang.System.out.println(String.format("Project with identifier \"%s\" already exists in the database!", project.getProjectIdentifier().toUpperCase()));
            throw new ProjectIdException(String.format("Project with identifier \"%s\" already exists in the database!", project.getProjectIdentifier().toUpperCase()));
        }
    }

    // SELECT from database function(s):
    @Override
    public Iterable<Project> findAllProjects(String username) {
        List<Project> allProjectsInDatabase = new ArrayList<>();

        for (Project project : projectRepository.findAllByProjectLeader(username)) {
            allProjectsInDatabase.add(project);
        }

        if (allProjectsInDatabase.isEmpty()) {
            java.lang.System.out.println(String.format("No projects exist in the database!"));
            throw new NoProjectsExistInDatabaseException(String.format("No projects exist in the database!"));
        } else {
            return projectRepository.findAllByProjectLeader(username);
        }
    }

    @Override
    public Project findById(Long id) {
        if (projectRepository.getById(id) != null) {
            return projectRepository.getById(id);
        } else {
            java.lang.System.out.println(String.format("Project with ID number %d is not found in the database!", id));
            throw new ProjectNotFoundException(String.format("Project with ID number %d is not found in the database!", id));
        }
    }

    @Override
    public Project findByIdentifier(String identifier, String username) {
        Project project = projectRepository.getByProjectIdentifier(identifier.toUpperCase());
        if (project != null) {
            if (!project.getProjectLeader().equals(username)) {
                throw new ProjectNotFoundException(String.format("Project with identifier \"%s\" is not found in your account!", identifier.toUpperCase()));
            } else {
                return projectRepository.getByProjectIdentifier(identifier.toUpperCase());
            }
        } else {
            java.lang.System.out.println(String.format("Project with identifier \"%s\" is not found in your account!", identifier.toUpperCase()));
            throw new ProjectIdException(String.format("Project with identifier \"%s\" is not found in your account!", identifier.toUpperCase()));
        }
    }

    @Override
    public Project findByName(String projectName) {
        if (projectRepository.getByProjectName(projectName) != null) {
            return projectRepository.getByProjectName(projectName);
        } else {
            java.lang.System.out.println(String.format("Project with name \"%s\" is not found in the database!", projectName));
            throw new ProjectNameException(String.format("Project with name \"%s\" is not found in the database!", projectName));
        }
    }

    @Override
    public Project findByDescription(String description) {
        if (projectRepository.getByDescription(description) != null) {
            return projectRepository.getByDescription(description);
        } else {
            java.lang.System.out.println(String.format("Project with description \"%s\" is not found in the database!", description));
            throw new ProjectDescriptionException(String.format("Project with description \"%s\" is not found in the database!", description));
        }
    }

    // DELETE from database function(s):
    @Override
    public void delete(Long id) {
        Project project = findById(id);
        try {
            projectRepository.delete(project);
        } catch (Exception exception) {
            java.lang.System.out.println(String.format("Project with ID number %d is not found in the database!", id));
            throw new ProjectNotFoundException(String.format("Project with ID number %d is not found in the database!", id));
        }
    }

    @Override
    public void deleteByIdentifier(String identifier, String username) {
        Project project = findByIdentifier(identifier.toUpperCase(), username);
        try {
            projectRepository.delete(project);
        } catch (Exception exception) {
            java.lang.System.out.println(String.format("Project with identifier \"%s\" already exists in your account!", project.getProjectIdentifier().toUpperCase()));
            throw new ProjectIdException(String.format("Project with identifier \"%s\" already exists in your account!", project.getProjectIdentifier().toUpperCase()));
        }
    }

    @Override
    public void deleteByProjectName(String projectName) {
        Project project = findByName(projectName);
        try {
            projectRepository.delete(project);
        } catch (Exception exception) {
            java.lang.System.out.println(String.format("Project with name \"%s\" is not found in the database!", projectName));
            throw new ProjectNameException(String.format("Project with name \"%s\" is not found in the database!", projectName));
        }
    }

    @Override
    public void deleteByDescription(String description) {
        Project project = findByDescription(description);
        try {
            projectRepository.delete(project);
        } catch (Exception exception) {
            java.lang.System.out.println(String.format("Project with description \"%s\" is not found in the database!", description));
            throw new ProjectDescriptionException(String.format("Project with description \"%s\" is not found in the database!", description));
        }
    }
}