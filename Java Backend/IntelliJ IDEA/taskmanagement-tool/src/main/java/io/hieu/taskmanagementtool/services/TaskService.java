package io.hieu.taskmanagementtool.services;

import io.hieu.taskmanagementtool.domain.Task;

public interface TaskService {
    Task addTask(String projectIdentifier, Task task, String username);

    Iterable<Task> findBacklogById(String id, String username);

    Task findTaskByProjectSequence(String backlog_id, String task_id, String username);

    Task updateByProjectSequence(Task updatedTask, String backlog_id, String task_id, String username);

    void deleteTaskByProjectSequence(String backlog_id, String task_id, String username);
}