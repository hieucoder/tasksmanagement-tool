package io.hieu.taskmanagementtool.security;

import io.hieu.taskmanagementtool.domain.User;
import io.hieu.taskmanagementtool.services.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

import static io.hieu.taskmanagementtool.security.SecurityConstants.HTTP_HEADER_STRING;
import static io.hieu.taskmanagementtool.security.SecurityConstants.JWT_PREFIX;

public class JsonWebTokenAuthenticationFilter extends OncePerRequestFilter {
    @Autowired
    private JsonWebTokenProvider jsonWebTokenProvider;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Override
    protected void doFilterInternal(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            FilterChain filterChain) throws ServletException, IOException {
        try {
            String jsonWebToken = getJsonWebTokenFromRequest(httpServletRequest); // Get JSON Web Token from HTTP Request.
            if (StringUtils.hasText(jsonWebToken) && jsonWebTokenProvider.validateJSONWebToken(jsonWebToken)) {
                Long userId = jsonWebTokenProvider.getUserIdFromJsonWebToken(jsonWebToken);
                User userDetails = customUserDetailsService.loadUserById(userId);

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, Collections.emptyList()
                );

                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception exception) {
            logger.error("Could not set user authentication in Spring security context!" + exception.getMessage());
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String getJsonWebTokenFromRequest(HttpServletRequest httpServletRequest) {
        String bearerJsonWebToken = httpServletRequest.getHeader(HTTP_HEADER_STRING); // Get the value of Authorization Header from the HTTP Request Headers.
        if (StringUtils.hasText(bearerJsonWebToken) && bearerJsonWebToken.startsWith(JWT_PREFIX)) { // If its value contains text and starts with "Bearer " then do:
            return bearerJsonWebToken.substring(7, bearerJsonWebToken.length()); // Return all the characters after the "Bearer " string.
        }
        return null; // Otherwise, return null as there was no JSON Web Token exists in Authorization Header.
    }
}