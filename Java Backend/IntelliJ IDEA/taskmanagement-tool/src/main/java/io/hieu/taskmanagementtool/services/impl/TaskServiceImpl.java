package io.hieu.taskmanagementtool.services.impl;

import io.hieu.taskmanagementtool.domain.Backlog;
import io.hieu.taskmanagementtool.domain.Project;
import io.hieu.taskmanagementtool.domain.Task;
import io.hieu.taskmanagementtool.exceptions.ProjectNotFoundException;
import io.hieu.taskmanagementtool.repositories.BacklogRepository;
import io.hieu.taskmanagementtool.repositories.ProjectRepository;
import io.hieu.taskmanagementtool.repositories.TaskRepository;
import io.hieu.taskmanagementtool.services.ProjectService;
import io.hieu.taskmanagementtool.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private BacklogRepository backlogRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectService projectService;

    @Override
    public Task addTask(String projectIdentifier, Task task, String username) {
        Backlog backlog = projectService.findByIdentifier(projectIdentifier, username).getBacklog(); // backlogRepository.findByProjectIdentifier(projectIdentifier);
        task.setBacklog(backlog);

        Integer backlogSequence = backlog.getProjectTaskSequence();
        backlogSequence++;
        backlog.setProjectTaskSequence(backlogSequence);

        task.setProjectSequence(String.format("%s - %s", projectIdentifier, backlogSequence));
        task.setProjectIdentifier(projectIdentifier);

        if (task.getStatus() == "" || task.getStatus() == null) {
            task.setStatus("TODO");
        }

        if (task.getPriority() == null || task.getPriority() == 0) {
            task.setPriority(3);
        }

        return taskRepository.save(task);
    }

    @Override
    public Iterable<Task> findBacklogById(String id, String username) {
        Project project = projectService.findByIdentifier(id, username);

        if (project == null) {
            throw new ProjectNotFoundException(String.format("Project with identifier \"%s\" is not found in the database!", id.toUpperCase()));
        }

        return taskRepository.findByProjectIdentifierOrderByPriority(id);
    }

    @Override
    public Task findTaskByProjectSequence(String backlog_id, String task_id, String username) {
        // Make sure that we are searching on the right backlog:
        projectService.findByIdentifier(backlog_id, username);

        // Make sure out tasks exists:
        Task task = taskRepository.findByProjectSequence(task_id);
        if (task == null) {
            throw new ProjectNotFoundException(String.format("Task with ID number: %s is not found in the database!", task_id));
        }

        // Make sure that the backlog/project id in the path corresponds to the right project:
        if (!task.getProjectIdentifier().equals(backlog_id)) {
            throw new ProjectNotFoundException(String.format("Task with ID number: %s does not exists in project: %s!", task_id, backlog_id));
        }

        return task;
    }

    @Override
    public Task updateByProjectSequence(Task updatedTask, String backlog_id, String task_id, String username) {
        Task task = findTaskByProjectSequence(backlog_id, task_id, username);
        task = updatedTask;
        return taskRepository.save(task);
    }

    @Override
    public void deleteTaskByProjectSequence(String backlog_id, String task_id, String username) {
        Task task = findTaskByProjectSequence(backlog_id, task_id, username);
        taskRepository.delete(task);
    }
}