package io.hieu.taskmanagementtool.repositories

import io.hieu.taskmanagementtool.domain.Project
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProjectRepository : CrudRepository<Project?, Long?> {
    fun getById(id: Long?): Project?

    fun getByProjectIdentifier(identifier: String?): Project?

    fun getByProjectName(projectName: String?): Project?

    fun getByDescription(description: String?): Project?

    override fun findAll(): Iterable<Project?>

    fun findAllByProjectLeader(username: String?): Iterable<Project?>?
}