package io.hieu.taskmanagementtool.services

import io.hieu.taskmanagementtool.domain.Task

interface TaskService {
    fun addTask(projectIdentifier: String?, task: Task?, username: String?): Task?

    fun findBacklogById(id: String?, username: String?): Iterable<Task?>?

    fun findTaskByProjectSequence(backlog_id: String?, task_id: String?, username: String?): Task?

    fun updateByProjectSequence(updatedTask: Task?, backlog_id: String?, task_id: String?, username: String?): Task?

    fun deleteTaskByProjectSequence(backlog_id: String?, task_id: String?, username: String?)
}