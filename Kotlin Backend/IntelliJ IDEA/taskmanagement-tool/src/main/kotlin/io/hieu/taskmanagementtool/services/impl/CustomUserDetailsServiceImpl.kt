package io.hieu.taskmanagementtool.services.impl

import io.hieu.taskmanagementtool.domain.User
import io.hieu.taskmanagementtool.repositories.UserRepository
import io.hieu.taskmanagementtool.services.CustomUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CustomUserDetailsServiceImpl : CustomUserDetailsService {
    @Autowired
    private val userRepository: UserRepository? = null

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository!!.findByUsername(username)
        if (user == null) {
            UsernameNotFoundException(String.format("User with username %s is not found in the database!", username))
        }
        return user!!
    }

    @Transactional
    override fun loadUserById(id: Long): User {
        val user = userRepository!!.getById(id)
        if (user == null) {
            UsernameNotFoundException(String.format("User with ID number %d not found in the database!", id))
        }
        return user!!
    }
}