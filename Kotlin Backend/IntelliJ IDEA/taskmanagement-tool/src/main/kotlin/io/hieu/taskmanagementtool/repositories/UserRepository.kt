package io.hieu.taskmanagementtool.repositories

import io.hieu.taskmanagementtool.domain.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : CrudRepository<User?, Long?> {
    fun findByUsername(username: String?): User?

    fun getById(id: Long?): User?
}