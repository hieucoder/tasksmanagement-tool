package io.hieu.taskmanagementtool.web

import io.hieu.taskmanagementtool.domain.Task
import io.hieu.taskmanagementtool.services.ErrorValidationService
import io.hieu.taskmanagementtool.services.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/api/backlogs", "/api/backlogs.html", "/api/backlogs/index", "/api/backlogs/index.html"])
@CrossOrigin
class BacklogController {
    @Autowired
    private val taskService: TaskService? = null

    @Autowired
    private val errorValidationService: ErrorValidationService? = null

    @PostMapping(path = ["/{backlog_id}", "/{backlog_id}/", "/{backlog_id}/index", "/{backlog_id}/index/", "/{backlog_id}/index.html", "/{backlog_id}/index.html/"])
    fun addTaskToBacklog(@RequestBody task: @Valid Task?, bindingResult: BindingResult?, @PathVariable backlog_id: String?, principal: Principal): ResponseEntity<*> {
        val errors = errorValidationService!!.ErrorValidationService(bindingResult)
        if (errors != null) {
            return errors
        }
        val newTask = taskService!!.addTask(backlog_id, task, principal.name)
        return ResponseEntity(newTask, HttpStatus.CREATED)
    }

    @GetMapping(path = ["/{backlog_id}", "/{backlog_id}/", "/{backlog_id}/index", "/{backlog_id}/index/", "/{backlog_id}/index.html", "/{backlog_id}/index.html/"])
    fun getBacklog(@PathVariable backlog_id: String?, principal: Principal): Iterable<Task?>? {
        return taskService!!.findBacklogById(backlog_id, principal.name)
    }

    @GetMapping(path = ["/{backlog_id}/{task_id}", "/{backlog_id}/{task_id}/", "/{backlog_id}/{task_id}/index", "/{backlog_id}/{task_id}/index/", "/{backlog_id}/{task_id}/index.html", "/{backlog_id}/{task_id}/index.html/"])
    fun getTask(@PathVariable backlog_id: String?, @PathVariable task_id: String?, principal: Principal): ResponseEntity<*> {
        val task = taskService!!.findTaskByProjectSequence(backlog_id, task_id, principal.name)
        return ResponseEntity(task, HttpStatus.OK)
    }

    @PatchMapping(path = ["/{backlog_id}/{task_id}", "/{backlog_id}/{task_id}/", "/{backlog_id}/{task_id}/index", "/{backlog_id}/{task_id}/index/", "/{backlog_id}/{task_id}/index.html", "/{backlog_id}/{task_id}/index.html/"])
    fun updateTask(
            @RequestBody task: @Valid Task?,
            bindingResult: BindingResult?,
            @PathVariable backlog_id: String?,
            @PathVariable task_id: String?,
            principal: Principal): ResponseEntity<*> {
        val errors = errorValidationService!!.ErrorValidationService(bindingResult)
        if (errors != null) {
            return errors
        }
        val updatedTask = taskService!!.updateByProjectSequence(task, backlog_id, task_id, principal.name)
        return ResponseEntity(updatedTask, HttpStatus.OK)
    }

    @DeleteMapping(path = ["/{backlog_id}/{task_id}", "/{backlog_id}/{task_id}/", "/{backlog_id}/{task_id}/index", "/{backlog_id}/{task_id}/index/", "/{backlog_id}/{task_id}/index.html", "/{backlog_id}/{task_id}/index.html/"])
    fun deleteTask(@PathVariable backlog_id: String?, @PathVariable task_id: String?, principal: Principal): ResponseEntity<*> {
        taskService!!.deleteTaskByProjectSequence(backlog_id, task_id, principal.name)
        return ResponseEntity(String.format("Task with identifier %s is removed from the database!", task_id), HttpStatus.OK)
    }
}