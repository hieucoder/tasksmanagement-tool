package io.hieu.taskmanagementtool.services.impl

import io.hieu.taskmanagementtool.domain.Task
import io.hieu.taskmanagementtool.exceptions.ProjectNotFoundException
import io.hieu.taskmanagementtool.repositories.BacklogRepository
import io.hieu.taskmanagementtool.repositories.ProjectRepository
import io.hieu.taskmanagementtool.repositories.TaskRepository
import io.hieu.taskmanagementtool.services.ProjectService
import io.hieu.taskmanagementtool.services.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TaskServiceImpl : TaskService {
    @Autowired
    private val backlogRepository: BacklogRepository? = null

    @Autowired
    private val taskRepository: TaskRepository? = null

    @Autowired
    private val projectRepository: ProjectRepository? = null

    @Autowired
    private val projectService: ProjectService? = null
    override fun addTask(projectIdentifier: String?, task: Task?, username: String?): Task? {
        val backlog = projectService!!.findByIdentifier(projectIdentifier, username)!!.backlog // backlogRepository.findByProjectIdentifier(projectIdentifier);
        task!!.backlog = backlog
        var backlogSequence = backlog!!.projectTaskSequence
        backlogSequence++
        backlog.projectTaskSequence = backlogSequence
        task.projectSequence = String.format("%s - %s", projectIdentifier, backlogSequence)
        task.projectIdentifier = projectIdentifier
        if (task.status === "" || task.status == null) {
            task.status = "TODO"
        }
        if (task.priority == null || task.priority == 0) {
            task.priority = 3
        }
        return taskRepository!!.save(task)
    }

    override fun findBacklogById(id: String?, username: String?): Iterable<Task?>? {
        val project = projectService!!.findByIdentifier(id, username)
                ?: throw ProjectNotFoundException(String.format("Project with identifier \"%s\" is not found in the database!", id!!.toUpperCase()))
        return taskRepository!!.findByProjectIdentifierOrderByPriority(id)
    }

    override fun findTaskByProjectSequence(backlog_id: String?, task_id: String?, username: String?): Task? {
        // Make sure that we are searching on the right backlog:
        projectService!!.findByIdentifier(backlog_id, username)

        // Make sure out tasks exists:
        val task = taskRepository!!.findByProjectSequence(task_id)
                ?: throw ProjectNotFoundException(String.format("Task with ID number: %s is not found in the database!", task_id))

        // Make sure that the backlog/project id in the path corresponds to the right project:
        if (task.projectIdentifier != backlog_id) {
            throw ProjectNotFoundException(String.format("Task with ID number: %s does not exists in project: %s!", task_id, backlog_id))
        }
        return task
    }

    override fun updateByProjectSequence(updatedTask: Task?, backlog_id: String?, task_id: String?, username: String?): Task? {
        var task = findTaskByProjectSequence(backlog_id, task_id, username)
        task = updatedTask
        return taskRepository!!.save(task!!)
    }

    override fun deleteTaskByProjectSequence(backlog_id: String?, task_id: String?, username: String?) {
        val task = findTaskByProjectSequence(backlog_id, task_id, username)
        taskRepository!!.delete(task!!)
    }
}