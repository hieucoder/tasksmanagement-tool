package io.hieu.taskmanagementtool.services.impl

import io.hieu.taskmanagementtool.domain.Backlog
import io.hieu.taskmanagementtool.domain.Project
import io.hieu.taskmanagementtool.exceptions.*
import io.hieu.taskmanagementtool.repositories.BacklogRepository
import io.hieu.taskmanagementtool.repositories.ProjectRepository
import io.hieu.taskmanagementtool.repositories.UserRepository
import io.hieu.taskmanagementtool.services.ProjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProjectServiceImpl : ProjectService {
    @Autowired
    private val projectRepository: ProjectRepository? = null

    @Autowired
    private val backlogRepository: BacklogRepository? = null

    @Autowired
    private val userRepository: UserRepository? = null

    // INSERT data into database function(s):
    override fun saveOrUpdateProject(project: Project?, username: String?): Project? {
        if (project!!.id != null) {
            val existingProject = projectRepository!!.getByProjectIdentifier(project.projectIdentifier)
            if (existingProject != null && existingProject.projectLeader != username) {
                throw ProjectNotFoundException(String.format("Project with identifier %s is not found in your account!", project.projectIdentifier))
            } else if (existingProject == null) {
                throw ProjectNotFoundException(String.format("Project with identifier %s is not found in your account!", project.projectIdentifier))
            }
        }
        return try {
            val user = userRepository!!.findByUsername(username)
            project.user = user
            project.projectLeader = user!!.username
            project.projectIdentifier = project.projectIdentifier!!.toUpperCase()
            if (project.id == null) {
                val backlog = Backlog()
                project.backlog = backlog
                backlog.project = project
                backlog.projectIdentifier = project.projectIdentifier!!.toUpperCase()
            }
            if (project.id != null) {
                project.backlog = backlogRepository!!.findByProjectIdentifier(project.projectIdentifier!!.toUpperCase())
            }
            projectRepository!!.save(project)
        } catch (exception: Exception) {
            println(String.format("Project with identifier \"%s\" already exists in the database!", project.projectIdentifier!!.toUpperCase()))
            throw ProjectIdException(String.format("Project with identifier \"%s\" already exists in the database!", project.projectIdentifier!!.toUpperCase()))
        }
    }

    // SELECT from database function(s):
    override fun findAllProjects(username: String?): Iterable<Project?>? {
        val allProjectsInDatabase: MutableList<Project?> = ArrayList()
        for (project in projectRepository!!.findAllByProjectLeader(username)!!) {
            allProjectsInDatabase.add(project)
        }
        return if (allProjectsInDatabase.isEmpty()) {
            println(String.format("No projects exist in the database!"))
            throw NoProjectsExistInDatabaseException(String.format("No projects exist in the database!"))
        } else {
            projectRepository.findAllByProjectLeader(username)
        }
    }

    override fun findById(id: Long?): Project? {
        return if (projectRepository!!.getById(id) != null) {
            projectRepository.getById(id)
        } else {
            println(String.format("Project with ID number %d is not found in the database!", id))
            throw ProjectNotFoundException(String.format("Project with ID number %d is not found in the database!", id))
        }
    }

    override fun findByIdentifier(identifier: String?, username: String?): Project? {
        val project = projectRepository!!.getByProjectIdentifier(identifier!!.toUpperCase())
        return if (project != null) {
            if (project.projectLeader != username) {
                throw ProjectNotFoundException(String.format("Project with identifier \"%s\" is not found in your account!", identifier.toUpperCase()))
            } else {
                projectRepository.getByProjectIdentifier(identifier.toUpperCase())
            }
        } else {
            println(String.format("Project with identifier \"%s\" is not found in your account!", identifier.toUpperCase()))
            throw ProjectIdException(String.format("Project with identifier \"%s\" is not found in your account!", identifier.toUpperCase()))
        }
    }

    override fun findByName(projectName: String?): Project? {
        return if (projectRepository!!.getByProjectName(projectName) != null) {
            projectRepository.getByProjectName(projectName)
        } else {
            println(String.format("Project with name \"%s\" is not found in the database!", projectName))
            throw ProjectNameException(String.format("Project with name \"%s\" is not found in the database!", projectName))
        }
    }

    override fun findByDescription(description: String?): Project? {
        return if (projectRepository!!.getByDescription(description) != null) {
            projectRepository.getByDescription(description)
        } else {
            println(String.format("Project with description \"%s\" is not found in the database!", description))
            throw ProjectDescriptionException(String.format("Project with description \"%s\" is not found in the database!", description))
        }
    }

    // DELETE from database function(s):
    override fun delete(id: Long?) {
        val project = findById(id)
        try {
            projectRepository!!.delete(project!!)
        } catch (exception: Exception) {
            println(String.format("Project with ID number %d is not found in the database!", id))
            throw ProjectNotFoundException(String.format("Project with ID number %d is not found in the database!", id))
        }
    }

    override fun deleteByIdentifier(identifier: String?, username: String?) {
        val project = findByIdentifier(identifier!!.toUpperCase(), username)
        try {
            projectRepository!!.delete(project!!)
        } catch (exception: Exception) {
            println(String.format("Project with identifier \"%s\" already exists in your account!", project!!.projectIdentifier!!.toUpperCase()))
            throw ProjectIdException(String.format("Project with identifier \"%s\" already exists in your account!", project.projectIdentifier!!.toUpperCase()))
        }
    }

    override fun deleteByProjectName(projectName: String?) {
        val project = findByName(projectName)
        try {
            projectRepository!!.delete(project!!)
        } catch (exception: Exception) {
            println(String.format("Project with name \"%s\" is not found in the database!", projectName))
            throw ProjectNameException(String.format("Project with name \"%s\" is not found in the database!", projectName))
        }
    }

    override fun deleteByDescription(description: String?) {
        val project = findByDescription(description)
        try {
            projectRepository!!.delete(project!!)
        } catch (exception: Exception) {
            println(String.format("Project with description \"%s\" is not found in the database!", description))
            throw ProjectDescriptionException(String.format("Project with description \"%s\" is not found in the database!", description))
        }
    }
}