package io.hieu.taskmanagementtool.web

import io.hieu.taskmanagementtool.domain.Project
import io.hieu.taskmanagementtool.services.ErrorValidationService
import io.hieu.taskmanagementtool.services.ProjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/api/projects", "/api/projects.html", "/api/projects/index", "/api/projects/index.html"])
@CrossOrigin
class ProjectController {
    @Autowired
    private val projectService: ProjectService? = null

    @Autowired
    private val errorValidationService: ErrorValidationService? = null

    // HTTP 1.1 POST request(s):
    @PostMapping(path = ["", "/"])
    fun createNewProject(
            @RequestBody project: @Valid Project?,
            bindingResult: BindingResult?,
            principal: Principal): ResponseEntity<*> {
        val errors = errorValidationService!!.ErrorValidationService(bindingResult)
        return if (errors != null) {
            errors
        } else {
            val newProject = projectService!!.saveOrUpdateProject(project, principal.name)
            println(String.format("Successfully created and stored a new project named \"%s\" to the database!", project!!.projectName))
            ResponseEntity(newProject, HttpStatus.CREATED)
        }
    }

    // HTTP 1.1 GET request(s):
    @GetMapping(path = ["", "/", "/all", "/all/", "/all/index", "/all/index/", "/all/index.html", "/all/index.html/"])
    fun getAllProjects(principal: Principal): Iterable<Project?>? {
        println(String.format("Retrieved all projects from the database!"))
        return projectService!!.findAllProjects(principal.name)
    }

    @GetMapping(path = ["/{project_id}", "/{project_id}/", "/{project_id}/index", "/{project_id}/index/", "/{project_id}/index.html", "/{project_id}/index.html/"])
    fun getProjectById(@PathVariable project_id: Long?): ResponseEntity<*> {
        val project = projectService!!.findById(project_id)
        println(String.format("Project with ID number %d retrieved from the database!", project_id))
        return ResponseEntity(project, HttpStatus.OK)
    }

    @GetMapping(path = ["/get-project-by-identifier/{project_identifier}", "/get-project-by-identifier/{project_identifier}/", "/get-project-by-identifier/{project_identifier}/index", "/get-project-by-identifier/{project_identifier}/index/", "/get-project-by-identifier/{project_identifier}/index.html", "/get-project-by-identifier/{project_identifier}/index.html/"])
    fun getProjectByIdentifier(@PathVariable project_identifier: String, principal: Principal): ResponseEntity<*> {
        val project = projectService!!.findByIdentifier(project_identifier, principal.name)
        println(String.format("Project with identifier \"%s\" retrieved from the database!", project_identifier.toUpperCase()))
        return ResponseEntity(project, HttpStatus.OK)
    }

    @GetMapping(path = ["/get-project-by-name/{project_name}", "/get-project-by-name/{project_name}/", "/get-project-by-name/{project_name}/index", "/get-project-by-name/{project_name}/index/", "/get-project-by-name/{project_name}/index.html", "/get-project-by-name/{project_name}/index.html/"])
    fun getProjectByName(@PathVariable project_name: String?): ResponseEntity<*> {
        val project = projectService!!.findByName(project_name)
        println(String.format("Project with name \"%s\" is retrieved from the database!", project_name))
        return ResponseEntity(project, HttpStatus.NOT_FOUND)
    }

    @GetMapping(path = ["/get-project-by-description/{project_description}", "/get-project-by-description/{project_description}/", "/get-project-by-description/{project_description}/index", "/get-project-by-description/{project_description}/index/", "/get-project-by-description/{project_description}/index.html", "/get-project-by-description/{project_description}/index.html/"])
    fun getProjectByDescription(@PathVariable project_description: String?): ResponseEntity<*> {
        val project = projectService!!.findByDescription(project_description)
        println(String.format("Project with description \"%s\" is retrieved from the database!", project_description))
        return ResponseEntity(project, HttpStatus.OK)
    }

    // HTTP 1.1 DELETE request(s):
    @DeleteMapping(path = ["/{project_id}", "/{project_id}/", "/{project_id}/index", "/{project_id}/index/", "/{project_id}/index.html", "/{project_id}/index.html/"])
    fun deleteProject(@PathVariable project_id: Long?): ResponseEntity<*> {
        projectService!!.delete(project_id)
        println(String.format("Project with ID number %d is removed from the database!", project_id))
        return ResponseEntity(String.format("Project with ID number %d is removed from the database!", project_id), HttpStatus.OK)
    }

    @DeleteMapping(path = ["/delete-project-by-identifier/{project_identifier}", "/delete-project-by-identifier/{project_identifier}/", "/delete-project-by-identifier/{project_identifier}/index", "/delete-project-by-identifier/{project_identifier}/index/", "/delete-project-by-identifier/{project_identifier}/index.html", "/delete-project-by-identifier/{project_identifier}/index.html/"])
    fun deleteProjectByIdentifier(@PathVariable project_identifier: String, principal: Principal): ResponseEntity<*> {
        projectService!!.deleteByIdentifier(project_identifier, principal.name)
        println(String.format("Project with identifier \"%s\" is removed from the database!", project_identifier.toUpperCase()))
        return ResponseEntity(String.format("Project with identifier \"%s\" is removed from the database!", project_identifier.toUpperCase()), HttpStatus.OK)
    }

    @DeleteMapping(path = ["/delete-project-by-name/{project_name}", "/delete-project-by-name/{project_name}/", "/delete-project-by-name/{project_name}/index", "/delete-project-by-name/{project_name}/index/", "/delete-project-by-name/{project_name}/index.html", "/delete-project-by-name/{project_name}/index.html/"])
    fun deleteProjectByName(@PathVariable project_name: String?): ResponseEntity<*> {
        projectService!!.deleteByProjectName(project_name)
        println(String.format("Project with project name: \"%s\" is removed from the database!", project_name))
        return ResponseEntity(String.format("Project with project name: \"%s\" is removed from the database!", project_name), HttpStatus.OK)
    }

    @DeleteMapping(path = ["/delete-project-by-description/{project_description}", "/delete-project-by-description/{project_description}/", "/delete-project-by-description/{project_description}/index", "/delete-project-by-description/{project_description}/index/", "/delete-project-by-description/{project_description}/index.html", "/delete-project-by-description/{project_description}/index.html/"])
    fun deleteProjectByDescription(@PathVariable project_description: String?): ResponseEntity<*> {
        projectService!!.deleteByDescription(project_description)
        println(String.format("Project with description: \"%s\" is removed from the database!", project_description))
        return ResponseEntity(String.format("Project with description: \"%s\" is removed from the database!", project_description), HttpStatus.OK)
    }
}