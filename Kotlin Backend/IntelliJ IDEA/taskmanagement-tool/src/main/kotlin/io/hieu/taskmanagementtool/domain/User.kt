package io.hieu.taskmanagementtool.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank

@Entity
class User : UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(unique = true)
    private var username: @Email(message = "Username needs to be your email address!") @NotBlank(message = "Username is required and can not be blank!") String? = null
    var fullName: @NotBlank(message = "Please enter your full name!") String? = null
    private var password: @NotBlank(message = "Password is required and can not be blank!") String? = null

    @Transient // Telling JPA not to persist this field.
    var confirmPassword: String? = null

    // JPA's OneToMany relationship mapping with Project:
    @OneToMany(cascade = [CascadeType.REFRESH], fetch = FetchType.EAGER, mappedBy = "user", orphanRemoval = true)
    var projects: List<Project> = ArrayList()
    var createAt: Date? = null
    var updateAt: Date? = null

    override fun getUsername(): String {
        return username!!
    }

    fun setUsername(username: String?) {
        this.username = username
    }

    override fun getPassword(): String {
        return password!!
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    @PrePersist
    protected fun onCreate() {
        createAt = Date()
    }

    @PreUpdate
    protected fun onUpdate() {
        updateAt = Date()
    }

    // Implementing Spring Security's User Details interface's methods:
    @JsonIgnore
    override fun getAuthorities(): Collection<GrantedAuthority?>? {
        return null
    }

    @JsonIgnore
    override fun isAccountNonExpired(): Boolean {
        return true
    }

    @JsonIgnore
    override fun isAccountNonLocked(): Boolean {
        return true
    }

    @JsonIgnore
    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    @JsonIgnore
    override fun isEnabled(): Boolean {
        return true
    }
}