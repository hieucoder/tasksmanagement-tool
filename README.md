# **Tasks Management Tool**

To start Tasks Management Tool web application:
 - Step 1: From your local computer, inside the directory where you have downloaded this project, navigate to one of the the Backend folders, run your operating system's Terminal or Command Prompt, enter and run the command: "java -jar target/taskmanagement-tool-0.0.1-SNAPSHOT.jar" to start up the backend server.
 - Step 2: Navigate to the ReactJS Frontend folder, run your operating system's Terminal or Command Prompt, enter and run the command: "npm start" to start up the frontend client and you will be automatically navigated to your favorite default browser on your operating system on your computer.

 Home Page:
<img src='./1.png'/>

Sign Up:
<img src='./2.png'/>
<img src='./3.png'/>

Log In:
<img src='./4.png'/>
<img src='./5.png'/>

Dashboard:
<img src='./6.png'/>

Project Board:
<img src='./7.png'/>
<img src='./8.png'/>
<img src='./9.png'/>

Tasks:
<img src='./10.png'/>
<img src='./11.png'/>
<img src='./12.png'/>
<img src='./13.png'/>
<img src='./14.png'/>

For further support, contact me at: 

- Email: hieucoder@outlook.com or hieu.minhle@nashtechglobal.com.
- Mobile phone: +84908109633
- Facebook: https://www.facebook.com/hieucomputerprogrammer
- LinkedIn: https://www.linkedin.com/in/hieucoder/

😊 Have a good day! 😊